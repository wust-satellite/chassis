# Chassis

CubeSat chassis was designed according to specifications defined in [architecture/chassis_design.md](https://gitlab.com/wust-satellite/architecture/-/blob/master/docs/chassis_design.md?ref_type=heads). Most of the design work was done in Onshape CAD program. It was chosen for its accesibility and ease of design sharing. All design files are open and available through the public Onshape project. 

[Project link](https://cad.onshape.com/documents/f2fb1647d1e320f50a71d409/w/86283d51a0d1631ea2af71e9/e/2640885cbccb014c60da0cc6?renderMode=0&uiState=6679bbd0de7c0722f9c68a85)

![CubeSat image](cubesat.gif) 

## Design and manufacturing considerations

To accelerate the design process project makes great use of common off-the-shelf parts and 3D printing. Price and availability were major driving factors in component choice. Similarly, fused filament fabrication allowed for rapid prototyping at low cost. 


## Off-the-shelf component list

- 4x MakerBeam 10x10x600mm rail
- MakerBeam T-slot nut set
- 4x 5x7x5mm bushing
- 200mm ϕ5 hardened steel rod
- 4x torsion spring (inner diameter >= 5mm, outer diameter <= 10mm, leg angle ~= 0°, torsional stiffness ~= 0.5 Nmm/°)
- fishing line
- assortment of M3 nuts and bolts


## Solar panel and antenna deployment mechanism

Both solar panels and communication antennas are held with fishing line while stowed. During deployment EPS module sends current through resistors touching the line, burning through the string. Inherent springiness of antennas and springs in solar panels unfold them to desired positions.

![CubeSat unfolded](cubesat_unfolded.png)

